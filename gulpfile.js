var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');



//scss->css im Ordner ./style
gulp.task('css', function() {
  gulp.src('./style/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./style'));
});


// gulp watch ruft task "css" auf
gulp.task('watch', function() {
  gulp.watch('./style/**/*.scss', ['css']);
});
